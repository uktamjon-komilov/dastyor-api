window.onload = () => {
    const all_countries = document.getElementById("id_country_code_0");
    const countries = document.querySelectorAll('[id^="id_country_code"]');

    countries.forEach((country) => {
        country.parentElement.parentElement.classList.add("fixed-country-width");
    });

    all_countries.addEventListener("change", () => {
        countries.forEach((country) => {
            country.checked = all_countries.checked;
        });
    });
}