from django.contrib import admin
from parler.admin import TranslatableAdmin

from api.models import StaticTranslation


admin.site.register(StaticTranslation, TranslatableAdmin)