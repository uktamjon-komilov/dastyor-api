from paycomuz.views import MerchantAPIView
from paycomuz import Paycom
from paycomuz.models import Transaction

from clients.models import Order


class CheckOrder(Paycom):
    def check_order(self, amount, account, *args, **kwargs):
        order = Order.objects.filter(
            id=account["order_id"],
            is_active=True,
            is_paid=False
        )

        if not order.exists():
            return self.ORDER_NOT_FOND

        order = order.first()

        if order.total_price_in_sum != amount:
            return self.INVALID_AMOUNT

        return self.ORDER_FOUND


    def successfully_payment(self, account, transaction, *args, **kwargs):
        order = Order.objects.filter(id=transaction.order_key)
        if order.exists():
            order = order.first()
            order.is_paid = True
            order.save()

    def cancel_payment(self, account, transaction, *args, **kwargs):
        print("account", account)
        print("transaction", transaction)
      

class PaymeAPIView(MerchantAPIView):
    VALIDATE_CLASS = CheckOrder