from rest_framework.response import Response
from rest_framework import status
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import action

from clients.models import Order


class StripeViewSet(ViewSet):
    authentication_classes = []
    
    @action(detail=False, methods=["post"], url_path="stripe/webhook")
    def payment_webhook(self, request):
        order_id = request.data["data"]["object"]["metadata"]["order_id"]
        print(order_id)
        order = Order.objects.filter(id=order_id)
        if order.exists():
            order = order.first()
            order.is_paid = True
            order.save()
        return Response(status=status.HTTP_200_OK)