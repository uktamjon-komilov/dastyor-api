from django.conf import settings
from clients.models import Order

class Stripe:
    def create_initialization(self, order):
        import stripe
        import math
        stripe.api_key = settings.STRIPE_SECRET_KEY
        domain_url = settings.FRONT_END_SITE_URL
        try:
            checkout_session = stripe.checkout.Session.create(
                success_url=domain_url + "payment-success?order_id="+str(order.id),
                cancel_url=domain_url + "payment-cancelled?order_id="+str(order.id),
                payment_method_types=["card"],
                mode="payment",
                line_items=[
                    {
                        "name": "{} {} {}".format(item.product, item.size, item.color),
                        "quantity": item.quantity,
                        "currency": "usd",
                        "amount": math.ceil(item.price),
                    }
                    for item in order.order_items.all()
                ],
                metadata={
                    "order_id": order.id
                }
            )
            return checkout_session.url
        except:
            return ""