from django.urls.conf import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from .views import *


router = DefaultRouter()
router.register("orders", OrderViewSet)
router.register("products", ProductViewSet)
router.register("product-rating", ProductRatingViewSet)
# router.register("payments", StripeViewSet, basename="payments")


urlpatterns = [
    path("login/", UserLoginApiView.as_view()),
    path("categories/", CategoryView.as_view()),
    path("banners/", BannerView.as_view()),

    path("client/", ClientCreateView.as_view()),
    path("client/<int:pk>/", ClientUpdateDestryView.as_view()),

    path("seller/", SellerCreateView.as_view()),
    path("seller/<int:pk>/", SellerUpdateDestryView.as_view()),
    path("seller/<str:unique_id>/products/", SellerProductsAPIView.as_view()),

    path("product-detail/", ProductDetailListView.as_view()),
    path("feature-group/", FeatureGroupListView.as_view()),
    path("translations/", TranslationsListView.as_view()),

    path("email-list/create/", EmailListItemCreateAPIView.as_view()),
    
    path("payments/payme/", PaymeAPIView.as_view()),
    path("payments/click/", ClickAPIView.as_view()),

    path("countries/", CountryFromIPView.as_view()),

    path("", include(router.urls)),

    path("token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]