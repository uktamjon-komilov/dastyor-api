from django.db import models
from parler.models import TranslatableModel, TranslatedFields


class StaticTranslation(TranslatableModel):
    key = models.CharField(max_length=255, unique=True)
    translations = TranslatedFields(
        value = models.CharField(max_length=255, null=True, blank=True)
    )

    def __str__(self):
        return self.safe_translation_getter("value") or self.key