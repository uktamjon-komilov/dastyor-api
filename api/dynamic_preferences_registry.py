from dynamic_preferences.types import FloatPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry


general = Section("general")


@global_preferences_registry.register
class Currency(FloatPreference):
    section = general
    name = "usd_to_uzs"
    default = 0.0