from django.conf import settings
from django.utils.translation import get_language_from_request
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from api.payments.stripe.core import Stripe
from clients.models import Order

from products.models import Category


class TranslatedSerializerMixin(object):

    def to_representation(self, instance):
        inst_rep = super().to_representation(instance)
        request = self.context.get("request", None) or self.request
        lang_code = get_language_from_request(request)
        result = {}
        for field_name, field in self.get_fields().items():
            if field_name != "translations":
                field_value = inst_rep.pop(field_name)
                result.update({field_name: field_value})
            if field_name == "translations":
                translations = inst_rep.pop(field_name)
                if lang_code not in translations:
                    parler_default_settings = settings.PARLER_LANGUAGES["default"]
                    if "fallback" in parler_default_settings:
                        lang_code = parler_default_settings.get("fallback")
                    if "fallbacks" in parler_default_settings:
                        lang_code = parler_default_settings.get("fallbacks")[0]
                for lang, translation_fields in translations.items():
                    if lang == lang_code:
                        trans_rep = translation_fields.copy()
                        for trans_field_name, trans_field in translation_fields.items():
                            field_value = trans_rep.pop(trans_field_name)
                            result.update({trans_field_name: field_value})
        return result


class DoSerializerMethod:
    def do_serialize(self, *args, **kwargs):
        if len(kwargs.keys()) == 0:
            kwargs = {}
        if kwargs.get("context", None):
            kwargs["context"]["request"] = self.request
        else:
            kwargs["context"] = {"request": self.request}
        if kwargs.get("serializer", None):
            serializer = kwargs.pop("serializer")
            return serializer(*args, **kwargs)
        return self.serializer_class(*args, **kwargs)


class ListModelViewSet(ModelViewSet):
    def create(self, request):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    
    def update(self, request, pk=None):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def partial_update(self, request, pk=None):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def destroy(self, request, pk=None):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class PaymentMethodsGenerator:
    data = {
        "name": None,
        "logo": None,
        "link": None
    }

    def get_payment_methods(self, order):
        result = [
            getattr(self, str(method))(order)
            for method in list(
                filter(
                    lambda x: str(x).startswith("payment_data_for_"),
                    dir(self)
                )
            )
        ]
        return result
    

    def payment_data_for_payme(self, order):
        from paycomuz import Paycom
        paycom = Paycom()
        url = paycom.create_initialization(
            amount=order.total_price_in_sum,
            order_id=str(order.id),
            return_url=settings.FRONT_END_SITE_URL
        )
        data = {**self.data}
        data["name"] = "Payme"
        data["link"] = url
        return data
    
    def payment_data_for_stripe(self, order):
        stripe = Stripe()
        data = {**self.data}
        data["name"] = "Stripe"
        data["link"] = stripe.create_initialization(order)
        return data
    
    def payment_data_for_click(self, order):
        from clickuz import ClickUz
        url = ClickUz.generate_url(
            order_id=str(order.id),
            amount=str(order.total_price_in_sum),
            return_url=settings.FRONT_END_SITE_URL
        )
        data = {**self.data}
        data["name"] = "Click"
        data["link"] = url
        return data