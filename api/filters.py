from django.db.models.aggregates import Sum
from clients.models import Order
from products.models import *
from django.db.models import Count, query


class ProductFilters:
    def min_filter(self, queryset):
        min_price = self.request.query_params.get("min_price", None)
        if min_price:
            queryset = queryset.filter(price__gte=float(min_price))
        
        return queryset
    
    
    def max_filter(self, queryset):
        max_price = self.request.query_params.get("max_price", None)
        if max_price:
            queryset = queryset.filter(price__lte=float(max_price))
        
        return queryset
    

    def search_filter(self, queryset):
        q = self.request.query_params.get("q", None)
        if q:
            q = q.strip()
            queryset = queryset.filter(translations__title__contains=q)
        
        return queryset
    

    def category_filter(self, queryset):
        q = self.request.query_params.get("category", None)
        if q:
            q = q.strip()
            category = Category.objects.filter(id=q)
            if category.exists():
                category = category.first()
                queryset = queryset.filter(category=category)
        
        return queryset
    

    def detail_filter(self, queryset):
        detail_ids = self.request.query_params.get("detail_ids", None)
        if not detail_ids:
            return queryset
        
        detail_ids = detail_ids.split(",")
        ids = []
        for id in detail_ids:
            try:
                id = int(id)
            except:
                id = 0
            
            if id != 0:
                ids.append(id)
        
        queryset = queryset.filter(details__id__in=ids)

        return queryset


    # Should be at the end
    def order_by_filter(self, queryset):
        order_by = self.request.query_params.get("order_by", None)
        if order_by == "most_viewed":
            queryset = queryset.annotate(num_views=Count("views")).order_by("-num_views")
        elif order_by == "oldest":
            queryset = queryset.order_by("created_at")
        elif order_by == "latest":
            queryset = queryset.order_by("-created_at")
        elif order_by == "best_seller":
            queryset = queryset.annotate(num_of_sales=Sum("order_items__quantity")).order_by("-num_of_sales")
        return queryset
    

    def apply_filters(self, queryset):
        filters = [
            getattr(self, attr)
            for attr in dir(self)
            if str(attr).endswith("_filter")
        ]
        for filter in filters:
            queryset = filter(queryset)
        return queryset.distinct()