from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet, ViewSet
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.settings import api_settings
from rest_framework.response import Response
from rest_framework.decorators import action
from django.utils.translation import gettext as _
from django.db import transaction
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_headers

from accounts.models import User
from api.constants import INVALID_ORDER_PRODUCT_DETAILS, USER_HAS_NO_CLIENT_ACCOUNT
from api.models import StaticTranslation
from api.utils import get_country_code_from_ip, get_ip_address
from products.models import Banner, Category, Product

from .serializers import *
from .permissions import *
from .pagination import *
from .mixins import *
from .filters import *

from .payments.payme.views import *
from .payments.stripe.views import *
from .payments.click.views import *


class UserFields:
    required_fields = ["email", "phone", "first_name", "last_name", "password"]

    def create_user(self, data):
        user = User.objects.create_user(
            email=data.get("email", ""),
            phone=data.get("phone", ""),
            first_name=data.get("first_name", ""),
            last_name=data.get("last_name", "")
        )

        return user


class ClientFields(UserFields):
    extra_fields = ["photo"]


class SellerFields(UserFields):
    extra_fields = ["shop_name", "fullname"]

    def  __init__(self):
        for field in self.extra_fields:
            self.required_fields.append(field)


class ClientCreateView(APIView, ClientFields):
    serializer_class = ClientSerializer

    def post(self, request):
        serializer = ClientSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        data = serializer.validated_data

        if not User.objects.filter(email=data["email"]).exists():
            with transaction.atomic():
                user = self.create_user(data)
                user.set_password(data["password"])
                user.save()
                client = Client(user=user)
                client.save()
                if request.data.get("photo", None):                    
                    filname = request.data["photo"].name
                    request.data["photo"].name = "user.{}".format(str(filname).split(".")[-1])
                    client.photo = request.data["photo"]
                    client.save()
                if client.photo:
                    photo_url = client.photo.url
                else:
                    photo_url = None
                return Response(
                    {
                        "id": client.id,
                        "email": client.user.email,
                        "phone": client.user.phone,
                        "first_name": client.user.first_name,
                        "last_name": client.user.last_name,
                        "photo": photo_url
                    },
                    status=status.HTTP_201_CREATED
                )
        else:
            return Response({
                "error": "A client with this email address already exists!"
            }, status=status.HTTP_400_BAD_REQUEST)


class ClientUpdateDestryView(APIView, ClientFields):
    def get_obj(self, pk):
        try:
            client = Client.objects.get(id=pk)
            return client
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def get(self, request, pk=None):
        client = Client.objects.filter(id=pk)
        if client.exists():
            client = client.first()
            serializer = ClientShortSerializer(client)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(
            {
            "detail": "Client not found."
            }, status=status.HTTP_404_NOT_FOUND)
        return Response(
            {
                "id": client.id,
                "email": client.user.email,
                "phone": client.user.phone,
                "first_name": client.user.first_name,
                "last_name": client.user.last_name
            },
            status=status.HTTP_200_OK
        )


    def patch(self, request, pk=None):
        client = self.get_obj(pk)
        data = request.data
        for key in self.required_fields:
            if data.get(key, None) is not None:
                if key == "password":
                    client.user.set_password(data[key])
                setattr(client.user, key, data[key])
        client.user.save()
        client.save()
        return Response(status=status.HTTP_202_ACCEPTED)


    def delete(self, request, pk=None):
        client = self.get_obj(pk)
        client.user.delete()
        client.delete()
        return Response(status=status.HTTP_202_ACCEPTED)


class SellerCreateView(APIView, SellerFields):

    def post(self, request):
        data = request.data

        if not User.objects.filter(email=data["email"]).exists():
            with transaction.atomic():
                user = self.create_user(data)
                user.set_password(data["password"])
                user.save()
                seller = Seller(
                    user=user,
                    fullname=data["fullname"],
                    shop_name=data["shop_name"]
                )
                seller.save()
                return Response(
                    {
                        "id": seller.id,
                        "email": seller.user.email,
                        "phone": seller.user.phone,
                        "first_name": seller.user.first_name,
                        "last_name": seller.user.last_name,
                        "fullname": seller.fullname,
                        "shop_name": seller.shop_name
                    },
                    status=status.HTTP_201_CREATED
                )
        else:
            return Response({
                "error": "A seller with this email address already exists!"
            }, status=status.HTTP_400_BAD_REQUEST)


class SellerUpdateDestryView(APIView, SellerFields):

    def get_obj(self, pk):
        try:
            seller = Seller.objects.get(id=pk)
            return seller
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def get(self, request, pk=None):
        seller = self.get_obj(pk)
        return Response(
            {
                "id": seller.id,
                "email": seller.user.email,
                "phone": seller.user.phone,
                "first_name": seller.user.first_name,
                "last_name": seller.user.last_name,
                "fullname": seller.fullname,
                "shop_name": seller.shop_name
            },
            status=status.HTTP_200_OK
        )


    def patch(self, request, pk=None):
        seller = self.get_obj(pk)
        data = request.data
        for key in self.required_fields:
            if data.get(key, None) is not None:
                if key == "password":
                    seller.user.set_password(data[key])
                setattr(seller.user, key, data[key])
        seller.user.save()
        seller.save()
        return Response(status=status.HTTP_202_ACCEPTED)


class UserLoginApiView(ObtainAuthToken):
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)
        user = token.user

        extra_data = {
            "user_id": user.id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "phone": user.phone,
            "balance": None,
            "shop_name": None,
            "fullname": None
        }

        if hasattr(user, "client"):
            extra_data["balance"] = user.client.balance
            extra_data["client_id"] = user.client.id
        elif hasattr(user, "seller"):
            extra_data["shop_name"] = user.seller.shop_name,
            extra_data["fullname"] = user.seller.fullname
            extra_data["seller_id"] = user.seller.id

        return Response({
            "token": token.key,
            **extra_data
        })


class CategoryView(APIView, DoSerializerMethod):
    serializer_class = CategorySerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES

    def get(self, request):
        result = []
        categories = Category.objects.all()
        for category in categories:
            category_ser = self.do_serialize(category)
            if category.children.all():
                result.append(category_ser.data)
                result[-1]["children"] = []
                for sub_category in category.children.all():
                    data = self.do_serialize(sub_category).data
                    data["children"] = None
                    result[-1]["children"].append(data)
            elif not category.parent:
                result.append(category_ser.data)
                result[-1]["children"] = None

        return Response(data=result)



class BannerView(APIView, DoSerializerMethod):
    serializer_class = BannerSerializer

    def get(self, request):
        banners = Banner.objects.all()
        serialized_banners = self.do_serialize(banners, many=True)
        return Response(data=serialized_banners.data)


class ProductViewSet(DoSerializerMethod, GenericViewSet, ProductFilters):
    serializer_class = ProductSerializer
    queryset = Product.objects.filter(is_available=True)
    pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        ip = get_ip_address(self.request)
        print(ip)
        data = get_country_code_from_ip(ip)
        if data["status"]:
            queryset = self.queryset.filter(country_code__icontains=data["country_code"])
            return queryset
        else:
            return self.queryset.filter(id=0)

    def return_paginated_response(self, queryset, extra_data={}):
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.do_serialize(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.do_serialize(page, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


    def get_obj_by_pk_or_slug(self, pk):
        queryset = self.get_queryset()
        try:
            pk = int(pk)
            product = queryset.filter(id=pk)
        except:
            product = queryset.filter(slug=pk)
        return product
    

    def retrieve(self, request, pk=None, *args, **kwargs):
        product = self.get_obj_by_pk_or_slug(pk)
        if product.exists():
            product = product.first()
            serializer = self.do_serialize(product)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
    
    def list(self, request):
        queryset = self.get_queryset()
        queryset = self.apply_filters(queryset)
        return self.return_paginated_response(queryset)


    @action(detail=False, methods=["get"], url_path="top-seller")
    def top_seller(self, request):
        queryset = self.get_queryset()
        queryset = self.apply_filters(queryset)
        return self.return_paginated_response(queryset)


    @action(detail=False, methods=["get"], url_path="most-common")
    def most_common(self, request):
        queryset = self.get_queryset()
        queryset = self.apply_filters(queryset)
        return self.return_paginated_response(queryset)


    @action(detail=True, methods=["get"], url_path="by-seller")
    def by_seller(self, request, pk=None):
        queryset = self.get_queryset()
        queryset = queryset.filter(seller__id=pk)
        queryset = self.apply_filters(queryset)
        return self.return_paginated_response(queryset)


    @action(detail=False, methods=["get"], url_path="latest")
    def latest(self, request):
        queryset = self.get_queryset()
        queryset = self.apply_filters(queryset)
        queryset = queryset.order_by("-updated_at")
        return self.return_paginated_response(queryset)
    

    @action(detail=True, methods=["get"], url_path="viewed")
    def viewed(self, request, pk=None):
        ip = get_ip_address(request)
        data = get_country_code_from_ip(ip)
        if data["status"]:
            product_view = ProductView.objects.filter(
                product__id=pk,
                ip=ip,
                country_code=data["country_code"]
            )
        else:
            product_view = ProductView.objects.filter(
                product__id=pk,
                ip=ip
            )
        if not product_view.exists():
            product = Product.objects.get(id=pk)
            product_view = ProductView(product=product, ip=ip)
            product_view.save()
        return Response(
            {
                "status": True,
                "detail": "PRODUCT_VIEWED"
            },
            status=status.HTTP_200_OK
        )
    

    def get_category_by_id_or_slug(self, pk):
        try:
            id = int(pk)
            category = Category.objects.filter(id=id)
            if category.exists():
                category = category.first()
            else:
                category = None
        except:
            category = Category.objects.filter(slug=pk)
            if category.exists():
                category = category.first()
            else:
                category = None
        return category
    

    def get_product_by_id_or_slug(self, pk):
        try:
            id = int(pk)
            product = Product.objects.filter(id=id)
        except:
            product = Product.objects.filter(slug=pk)
        return product


    @action(detail=True, methods=["get"], url_path="categorized")
    def by_seller(self, request, pk=None):
        category = self.get_category_by_id_or_slug(pk)
        if category:
            category_ids = [category.id]
            for child in category.children.all():
                category_ids.append(child.id)
            queryset = self.get_queryset().filter(category__id__in=category_ids)
            queryset = self.apply_filters(queryset)            
            return self.return_paginated_response(queryset)
        return Response(status=status.HTTP_404_NOT_FOUND)


    @action(detail=True, methods=["get"], url_path="extrimal-prices")
    def extrimal_prices(self, request, pk=None):
        category = self.get_category_by_id_or_slug(pk)
        if category:
            category_ids = [category.id]
            for child in category.children.all():
                category_ids.append(child.id)
            queryset = self.get_queryset().filter(category__id__in=category_ids)
            queryset = self.apply_filters(queryset)
            return Response(
            {
                "highest_price": queryset.order_by("-price").first().price,
                "lowest_price": queryset.order_by("price").first().price
            }, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)
    

    @action(detail=True, methods=["get"], url_path="related")
    def related(self, request, pk):
        product = self.get_obj_by_pk_or_slug(pk)
        if product.exists():
            product = product.first()
            queryset = self.get_queryset()
            queryset = queryset.exclude(seller=None)
            if product.seller:
                queryset = queryset.filter(seller__id=product.seller.id)
            queryset = queryset.order_by("-updated_at")
            queryset = queryset.exclude(id=product.id)[:15]
            return self.return_paginated_response(queryset)
        return Response(status=status.HTTP_404_NOT_FOUND)
    

    @action(detail=True, methods=["get"], url_path="reviews")
    def reviews(self, request, pk=None):
        product = self.get_product_by_id_or_slug(pk)
        if not product.exists():
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            product = product.first()
        reviews = product.ratings.all()
        serializer = ProductRelatedRatingSerializer(reviews, many=True)
        data = serializer.data
        return Response({
            "count": reviews.count(),
            "results": list(data)
        }, status=status.HTTP_200_OK)


    @action(detail=True, methods=["get"], url_path="features")
    def features(self, request, pk=None):
        serializer_class = FeatureGroupSerializer
        product = self.get_product_by_id_or_slug(pk)
        if not product.exists():
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            product = product.first()
        if not product.category:
            return Response(status=status.HTTP_404_NOT_FOUND)
        queryset = FeatureGroup.objects.filter(
            product_details__category__in=[product.category],
            product_details__id__in=[detail.id for detail in product.details.all()]
        ).distinct()
        serializer = self.do_serialize(
            queryset,
            many=True,
            serializer=serializer_class,
            product=product
        )
        return Response(serializer.data)


class ProductRatingViewSet(ModelViewSet):
    serializer_class = ProductRatingSerializer
    queryset = ProductRating.objects.all()
    authentication_classes = (JWTAuthentication,)
    permission_classes = [IsAuthenticated,]

    def create(self, request, *args, **kwargs):
        if not request.user.client:
            return Response({
                "short_message": USER_HAS_NO_CLIENT_ACCOUNT,
                "message": "User has no client account."
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            vdata = serializer.validated_data
            product = vdata["product"]
            client = request.user.client
            print(client)
            rating_score = vdata["rating"]
            rating = ProductRating.objects.filter(client=client, product=product)
            if rating.exists():
                rating = rating.first()
                rating.rating = rating_score
                rating.text = vdata.get("text") or None
            else:
                rating = ProductRating(
                    client=client,
                    product=product,
                    text=vdata.get("text") or None,
                    rating=vdata["rating"]
                )
            rating.save()
            rating_serializer = self.serializer_class(rating)
            return Response(rating_serializer.data, status=status.HTTP_200_OK)
                
        return Response(serializer.errors)


class ProductDetailListView(DoSerializerMethod, ListAPIView):
    serializer_class = ProductDetailSerializer
    queryset = ProductDetail.objects.all()

    def get(self, request):
        queryset = self.get_queryset()
        serializer = self.do_serialize(queryset, many=True)
        return Response(serializer.data)


class FeatureGroupListView(DoSerializerMethod, ListAPIView):
    serializer_class = FeatureGroupSerializer
    queryset = FeatureGroup.objects.all()

    def get(self, request):
        queryset = self.get_queryset()

        category = self.request.query_params.get("category", None)
        if category:
            try:
                id = int(category)
                queryset = queryset.filter(product_details__category__id=id)
            except:
                queryset = queryset.filter(product_details__category__slug=category)
            ids = []
            
            for item in queryset:
                ids.append(item.id)

            queryset = self.queryset.filter(id__in=ids)

        serializer = self.do_serialize(
            queryset,
            many=True,
            serializer=self.serializer_class,
        )
        return Response(serializer.data)


class OrderViewSet(ViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    authentication_classes = [JWTAuthentication]


    @action(detail=False, methods=["post"], url_path="create")
    def create_order(self, request):
        data = request.data
        order_serializer = OrderSerializer(data=data)
        if not order_serializer.is_valid() or not data.get("items", None):
            return Response(
                order_serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )
        for item in data["items"]:
            item_serializer = OrderProductSerializer(data=item)
            if not item_serializer.is_valid():
                return Response(item_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        try:
            with transaction.atomic():
                order = order_serializer.save()
                for item in data["items"]:
                    item_serializer = OrderProductSerializer(data=item)
                    if item_serializer.is_valid():
                        vdata = item_serializer.validated_data
                        order_product = OrderProduct.objects.filter(
                            order=order,
                            product=vdata["product"]
                        )

                        size_id = vdata.get("size", None)
                        if size_id:
                            order_product = order_product.filter(size__id=size_id)
                        else:
                            order_product = order_product.filter(size__isnull=True)
                        
                        color_id = vdata.get("color", None)
                        if color_id:
                            order_product = order_product.filter(color__id=color_id)
                        else:
                            order_product = order_product.filter(color__isnull=True)
                        

                        if order_product.exists():
                            order_product = order_product.first()
                            print(order_product)
                        else:
                            order_product = OrderProduct(
                                order=order,
                                product=vdata["product"],
                                quantity=vdata["quantity"]
                            )
                            if size_id:
                                size = Size.objects.get(id=size_id)
                                order_product.size = size
                            if color_id:
                                color = Color.objects.get(id=color_id)
                                order_product.color = color
                            order_product.save()

            order = Order.objects.get(id=order.id)
            serializer = OrderSerializer(order)
            return Response(serializer.data, status.HTTP_200_OK)

        except Exception as e:
            return Response(
            {
                "code": INVALID_ORDER_PRODUCT_DETAILS,
                "message": str(e)
            }, status=status.HTTP_400_BAD_REQUEST)
    

    @action(detail=False, methods=["get"], url_path="my-orders")
    def my_orders(self, request):
        if not request.user.is_authenticated:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        if hasattr(request.user, "seller") and request.user.seller:
            return Response(
            {
                "detail": "You are a seller. You don't have a client account which means you cannot see your orders."
            }, status=status.HTTP_404_NOT_FOUND)
        if not hasattr(request.user, "client") or not request.user.client:
            return Response(
            {
                "detail": "Your registrations has not been completed. Please, create a client account."
            }, status=status.HTTP_404_NOT_FOUND)
        result = {
            "unpaid_orders": None,
            "other_orders": None
        }
        unpaid_orders = Order.objects.filter(status="UNPAID")
        if unpaid_orders.exists():
            data = OrderShortSerializer(unpaid_orders, many=True).data
            result["unpaid_orders"] = list(data)
        other_orders = Order.objects.all().exclude(status="UNPAID")
        if other_orders.exists():
            data = OrderShortSerializer(other_orders, many=True).data
            result["unpaid_orders"] = list(data)
        return Response(
            result,
            status=status.HTTP_200_OK
        )

class TranslationsListView(ListAPIView):
    serializer_class = StaticTranslationSerializer
    queryset = StaticTranslation.objects.all()
    pagination_class = None

    @method_decorator(cache_page(60*60*2))
    @method_decorator(vary_on_headers("Accept-Language"))
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        instance_result = self.serializer_class(
            queryset,
            many=True,
            context={"request": request}
        ).data
        result = {}
        for instance in list(instance_result):
            result[instance["key"]] = instance["value"]
        return Response(result, status=status.HTTP_200_OK)


class EmailListItemCreateAPIView(CreateAPIView):
    def post(self, request):
        serializer = EmailListItemSerializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data["email_address"]
            email_list = Email.objects.filter(email_address=email)
            if not email_list.exists():
                country_data = self.get_country_data(request)
                serializer.save(**country_data)
            else:
                email_list = email_list.first()
                serializer = EmailListItemSerializer(email_list)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )
    

    def get_country_data(self, request):
        ip = get_ip_address(request)
        data = get_country_code_from_ip(ip)
        if data["status"]:
            country_code = data["country_code"]
            country_en = data["country"]
        else:
            country_code = "Unknown"
            country_en = "Unknown"
        return {
            "country_code": country_code,
            "country_en": country_en
        }


class SellerProductsAPIView(DoSerializerMethod, ProductFilters, APIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.filter(is_available=True)
    pagination_class = LargeResultsSetPagination

    def get(self, request, unique_id):
        queryset = self.get_queryset()
        base_64_utility = Base64Utilty()
        seller_id = base_64_utility.invert(unique_id)
        print(unique_id)
        print(seller_id)
        queryset = queryset.filter(seller__id=seller_id)
        print(queryset)
        queryset = self.apply_filters(queryset)
        return self.return_paginated_response(queryset, request)
    

    def get_queryset(self):
        return self.queryset.all()
    

    def return_paginated_response(self, queryset, request):
        paginator = self.pagination_class()
        result_page = paginator.paginate_queryset(queryset, request)
        serializer = self.serializer_class(result_page, many=True, context={"request": request})
        return paginator.get_paginated_response(serializer.data)



class CountryFromIPView(APIView):
    @method_decorator(cache_page(60*60*2))
    @method_decorator(vary_on_headers("Accept-Country"))
    def get(self, request):
        ip = get_ip_address(request)
        data = get_country_code_from_ip(ip)
        if data["status"]:
            current = {}
            accept_country = request.headers.get("Accept-Country", None)
            accepted_countries = list(
                filter(
                    lambda c: c[0].lower() == accept_country,
                    [country for country in COUNTRIES_DATA.items()]
                )
            )
            if accept_country and len(accepted_countries) > 0:
                current["code"] = accepted_countries[0][0].lower()
                current["title"] = accepted_countries[0][1]
            else:
                current["code"] = data["country_code"]
                current["title"] = data["country"]

            result = {
                "current": current,
                "others": []
            }
            for country in COUNTRIES_DATA.items():
                result["others"].append({
                    "code": country[0].lower(),
                    "title": country[1]
                })

            return Response(result, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_404_NOT_FOUND)