from django.db.models.aggregates import Avg
from rest_framework import serializers
from parler_rest.serializers import TranslatableModelSerializer
from parler_rest.fields import TranslatedFieldsField
from django.utils.translation import gettext as _
from django.utils import timezone

from accounts.models import *
from api.models import StaticTranslation
from api.utils import Base64Utilty
from clients.models import *
from products.models import *
from .mixins import PaymentMethodsGenerator, TranslatedSerializerMixin


class ClientSerializer(serializers.ModelSerializer):
    email = serializers.CharField()
    phone = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    password = serializers.CharField()

    class Meta:
        model = Client
        fields = ["id", "email", "phone", "first_name", "last_name", "photo", "balance", "password"]

        extra_kwargs = {
            "password": {
                "write_only": True
            },
        }
    
    def validate(self, attrs):
        return super().validate(attrs)
    
    def get_photo(self, obj):
        if self.photo:
            return self.photo.url
        return None


class ClientShortSerializer(ClientSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()

    class Meta:
        model = Client
        fields = ["id", "first_name", "last_name", "photo"]
    
    def get_first_name(self, obj):
        return obj.user.first_name
    
    def get_last_name(self, obj):
        return obj.user.last_name

    def get_photo(self, obj):
        if obj.photo:
            return obj.photo.url
        return None


class CategorySerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Category)

    class Meta:
        model = Category
        fields = ["id", "translations", "slug", "icon", "children"]


class CategoryShortSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Category)

    class Meta:
        model = Category
        fields = ["id", "translations", "slug"]


class BannerSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Banner)

    class Meta:
        model = Banner
        fields = ["translations", "link"]



class ProductDescriptionSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=ProductDescription)

    class Meta:
        model = ProductDescription
        fields = ["translations"]


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ["photo"]


class SocialMediaLinksSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMediaLink
        fields = "__all__"


class ColorSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Color)

    class Meta:
        model = Color
        fields = "__all__"


class SizeSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Size)

    class Meta:
        model = Size
        fields = "__all__"


class SellerSerializer(serializers.ModelSerializer):
    unique_id = serializers.SerializerMethodField()
    average_rating = serializers.SerializerMethodField()
    total_products = serializers.SerializerMethodField()

    class Meta:
        model = Seller
        fields = ["unique_id", "shop_name", "fullname", "average_rating", "total_products"]
    
    def get_unique_id(self, obj):
        base_64_utility = Base64Utilty()
        return base_64_utility.convert(str(obj.id))
    
    def get_average_rating(self, obj):
        products = self.get_seller_products(obj).annotate(average_rating=Avg("ratings__rating")).all()
        sum = 0
        count = 0
        for product in products:
            if product.average_rating:
                sum += product.average_rating
                count += 1
        
        if count > 0:
            return sum / count
        return 0.0
    
    def get_total_products(self, obj):
        products = self.get_seller_products(obj)
        return products.count()
    

    def get_seller_products(self, obj):
        return Product.objects.filter(seller=obj, is_available=True)


class ProductSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Product)
    descriptions = ProductDescriptionSerializer(many=True)
    category = CategoryShortSerializer(many=False)
    photos = ProductImageSerializer(many=True)
    color = ColorSerializer(many=True)
    size = SizeSerializer(many=True)
    seller = SellerSerializer(many=False)
    rating = serializers.SerializerMethodField()
    discount = serializers.SerializerMethodField()
    discounted_price = serializers.SerializerMethodField()
    social_media_links = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = ["id", "translations", "slug", "category", "discount", "price", "discounted_price", "rating", "photo", "descriptions", "color", "size", "photos", "social_media_links", "seller"]
        extra_fields = {
            "seller": {
                "read_only": True
            }
        }
    

    def get_rating(self, obj):
        ratings = ProductRating.objects.filter(product__id=obj.id)
        return ratings.aggregate(models.Avg("rating"))["rating__avg"]
    
    def get_discount_obj(self, obj):
        now = timezone.now()
        today = "{}-{}-{}".format(now.year, now.month, now.day)
        discount = obj.discounts.filter(product=obj, expires_in__gt=today, is_active=True)
        if discount.exists():
            discount = discount.first()
            return discount
        return None

    def get_discount(self, obj):
        discount = self.get_discount_obj(obj)
        if discount:
            return discount.discount
        return None
    
    def get_discounted_price(self, obj):
        discount = self.get_discount_obj(obj)
        if discount:
            return obj.price * (100 - discount.discount) / 100
        return None
    
    def get_social_media_links(self, obj):
        links = SocialMediaLink.objects.all()
        serializer = SocialMediaLinksSerializer(links, many=True)
        result = serializer.data
        return result


class ProductRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductRating
        fields = ["id", "product", "text", "rating"]


class ProductRelatedRatingSerializer(serializers.ModelSerializer):
    client = serializers.SerializerMethodField()

    class Meta:
        model = ProductRating
        fields = ["id", "text", "rating", "client"]
    
    def get_client(self, obj):
        client = obj.client
        serializer = ClientShortSerializer(client)
        data = serializer.data
        return dict(data)


class ProductItemSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Product)

    class Meta:
        model = Product
        fields = ["id", "translations", "slug", "price", "photo"]


class OrderProductSerializer(serializers.ModelSerializer):
    total_price = serializers.SerializerMethodField()
    size = serializers.IntegerField()
    color = serializers.IntegerField()

    class Meta:
        model = OrderProduct
        fields = ["id", "quantity", "product", "size", "color", "total_price"]
        extra_kwargs = {"product": {"required": True}} 
    
    def get_total_price(self, obj):
        return obj.quantity * obj.price


class OrderProductDetailSerializer(OrderProductSerializer):
    total_price = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()
    color = serializers.SerializerMethodField()

    class Meta:
        model = OrderProduct
        fields = ["id", "quantity", "product", "size", "color", "total_price"]
        extra_kwargs = {"product": {"required": True}} 
    
    def get_total_price(self, obj):
        return obj.quantity * obj.price

    def get_size(self, obj):
        try:
            return obj.size.id
        except:
            return 0

    def get_color(self, obj):
        try:
            return obj.color.id
        except:
            return 0


class OrderSerializer(serializers.ModelSerializer):
    order_items = serializers.SerializerMethodField()
    payment_links = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ["id", "client", "fullname", "phone", "email", "address_line", "country", "region", "city", "postcode", "order_items", "payment_links"]
    
    def get_order_items(self, obj):
        items = obj.order_items.all()
        if items.exists():
            serializer = OrderProductDetailSerializer(items, many=True)
            data = serializer.data
            return list(data)
        return []
    

    def get_payment_links(self, obj):
        generator = PaymentMethodsGenerator()
        data = generator.get_payment_methods(obj)
        print(data)
        return data


class OrderShortSerializer(OrderSerializer):
    class Meta:
        model = Order
        fields = ["id", "first_name", "last_name", "phone", "order_items"]


class ProductDetailSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=ProductDetail)
    
    class Meta:
        model = ProductDetail
        fields = ["id", "translations", "feature", "category"]
    
    def __init__(self, *args, **kwargs):
        if kwargs.get("context", None):
            self.request = kwargs["context"].pop("request", None)
        return super(ProductDetailSerializer, self).__init__(*args, **kwargs)


class FeatureGroupSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=FeatureGroup)
    product_details = serializers.SerializerMethodField()

    class Meta:
        model = FeatureGroup
        fields = ["id", "translations", "product_details"]

    
    def __init__(self, *args, **kwargs):
        if kwargs.get("context", None):
            self.chosen_product = kwargs.pop("product", None)
            self.request = kwargs["context"].pop("request", None)
        return super(FeatureGroupSerializer, self).__init__(*args, **kwargs)
    

    def get_product_details(self, obj):
        if hasattr(self, "chosen_product") and self.chosen_product:
            details = ProductDetail.objects.filter(product=self.chosen_product, feature=obj)
        else:
            details = ProductDetail.objects.filter(feature=obj)
        serializer = ProductDetailSerializer(details, many=True, context={"request": self.request})
        data = serializer.data
        return list(data)


class StaticTranslationSerializer(TranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=StaticTranslation)

    class Meta:
        model = StaticTranslation
        fields = ["id", "key", "translations"]


class EmailListItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = ["id", "email_address", "country_code"]
        extra_kwargs = {
            "country_code": {
                "read_only": True
            }
        }
