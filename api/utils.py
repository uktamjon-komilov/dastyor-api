def get_ip_address(request):    
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip


def get_country_code_from_ip(ip):
    import requests as r
    import json
    url = f"http://ip-api.com/json/{ip}"
    response = r.get(url)
    if 200 <= response.status_code < 300:
        data = json.loads(response.content)
        status = data.get("status", None)
        if status == "fail":
            return {
                "status": False
            }
        return {
            "status": True,
            "country_code": data["countryCode"].lower(),
            "country": data["country"],
        }
    return {
        "status": False
    }


class Base64Utilty:
    Z_FILL = 10

    def convert(self, text):
        import base64
        return base64.b64encode(text.zfill(self.Z_FILL).encode("ascii"))
    
    def invert(self, text):
        import base64
        return int(base64.b64decode(text).decode("ascii"))