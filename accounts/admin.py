from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

# from .models import User

User = get_user_model()


class UserAdmin(admin.ModelAdmin):
    list_display = ["first_name", "last_name", "phone", "email"]
    list_display_links = ["first_name", "last_name", "phone", "email"]
    readonly_fields = ["password"]


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)