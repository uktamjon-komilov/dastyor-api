from multiselectfield import MultiSelectField
from parler.models import TranslatableModel, TranslatedFields
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.translation import gettext as _

from products.data import COUNTRIES_DATA
from .managers import TransTreeManager
from django.db import models
from django.contrib.auth import get_user_model
import uuid
import os


User = get_user_model()

class DateTimeMixin:
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

def get_file_path(instance, filename):
    ext = filename.split(".")[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join("images", filename)


class Category(MPTTModel, TranslatableModel):
    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")


    parent = TreeForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name="children")
    translations = TranslatedFields(
        title = models.CharField(_("Category name"), max_length=255, null=True),
    )
    icon = models.FileField(upload_to=get_file_path, null=True, blank=True)
    slug = models.SlugField(_("Slug"), max_length=255, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = TransTreeManager()

    def __str__(self):
        return self.safe_translation_getter("title", any_language=True) or ""

    def __unicode__(self):
        return self.safe_translation_getter("title", any_language=True) or ""


class Seller(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="seller")
    fullname = models.CharField(max_length=255)
    shop_name = models.CharField(max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.shop_name


class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="client")
    photo = models.FileField(null=True, blank=True)
    balance = models.FloatField(default=0.0, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} {}".format(self.user.first_name, self.user.last_name)


class FeatureGroup(TranslatableModel):
    translations = TranslatedFields(
        title = models.CharField(_("Feature"), max_length=255),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.safe_translation_getter("title", any_language=True)


class ProductDetail(TranslatableModel):
    translations = TranslatedFields(
        value = models.CharField(_("Value"), max_length=255),
    )

    feature = models.ForeignKey(FeatureGroup, on_delete=models.CASCADE, blank=True, null=True, related_name="product_details")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="details")

    def __str__(self):
        return self.safe_translation_getter("value", any_language=True)


class Color(TranslatableModel):
    translations = TranslatedFields(
        title = models.TextField(_("Title"))
    )

    hex_code = models.CharField(max_length=16)

    def __str__(self):
        return self.safe_translation_getter("title", any_language=True) or ""


class Size(TranslatableModel):
    translations = TranslatedFields(
        title = models.TextField(_("Title"), null=True, blank=True)
    )
    value = models.CharField(max_length=255)

    def __str__(self):
        return self.value or self.safe_translation_getter("title", any_language=True) or ""


class Product(TranslatableModel):
    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    COUNTRIES = [
        ("all", _("All")),
        *[(country[0].lower(), country[1]) for country in COUNTRIES_DATA.items()]
    ]

    translations = TranslatedFields(
        title = models.CharField(_("Product name"), max_length=255),
    )
    
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE, null=True, related_name="products")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name=_("Category"))
    price = models.FloatField(default=0.0, verbose_name=_("Price"))
    is_available = models.BooleanField(default=True, verbose_name=_("Availability"))
    color = models.ManyToManyField(Color)
    size = models.ManyToManyField(Size)

    photo = models.ImageField(upload_to=get_file_path, null=True, verbose_name=_("Main image"))
    slug = models.CharField(max_length=255)

    details = models.ManyToManyField(ProductDetail, blank=True)

    country_code = MultiSelectField(choices=COUNTRIES)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.safe_translation_getter("title", any_language=True)

    def __unicode__(self):
        return self.safe_translation_getter("title", any_language=True)


class ProductDescription(TranslatableModel):
    translations = TranslatedFields(
        description = models.TextField(_("Description"))
    )

    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name=_("Product"), related_name="descriptions")


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name=_("Product"), related_name="photos")
    photo = models.FileField(upload_to=get_file_path, null=True, verbose_name=_("Extra photo"))


class ProductRating(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name=_("Product"), related_name="ratings")
    text = models.TextField(null=True, blank=True)
    rating = models.FloatField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if self.rating > 5:
            self.rating = 5.0
        elif self.rating < 0:
            self.rating = 0.0
        super(ProductRating, self).save(*args, **kwargs)


class Discount(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="discounts")
    discount = models.FloatField(default=0.0)
    expires_in = models.DateField()
    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        discount = Discount.objects.filter(product=self.product, is_active=True, expires_in__gt=self.expires_in)
        if discount.exists():
            for dis in discount:
                dis.is_active = False
                dis.save()
        return super(Discount, self).save(*args, **kwargs)


class ProductView(models.Model):
    ip = models.CharField(max_length=255)
    country_code = models.CharField(max_length=255, default="all")
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, related_name="views")


class Banner(TranslatableModel):
    class Meta:
        verbose_name = _("Banner")
        verbose_name_plural = _("Banners")

    translations = TranslatedFields(
        photo = models.ImageField(upload_to=get_file_path, verbose_name=_("Banner image"))
    )
    link = models.TextField()


class SocialMediaLink(models.Model):
    title = models.CharField(max_length=255)
    icon = models.FileField(upload_to="icons/")
    link = models.TextField()
    color = models.CharField(max_length=8)
    onHover = models.CharField(max_length=8)

    def __str__(self):
        return self.title