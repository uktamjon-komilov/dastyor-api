from parler.managers import TranslatableManager, TranslatableQuerySet
from mptt.managers import TreeManager
from mptt.querysets import TreeQuerySet


class TransTreeQuerySet(TranslatableQuerySet, TreeQuerySet):

    def as_manager(cls):
        manager = TransTreeManager.from_queryset(cls)()
        manager._built_with_as_manager = True
        return manager

    as_manager.queryset_only = True
    as_manager = classmethod(as_manager)


class TransTreeManager(TreeManager, TranslatableManager):
    _queryset_class = TransTreeQuerySet