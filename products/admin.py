from django.contrib import admin
from django import forms
from parler.admin import TranslatableAdmin, TranslatableTabularInline
from parler.forms import TranslatableModelForm
from mptt.admin import MPTTModelAdmin
from mptt.forms import MPTTAdminForm
from django.utils.html import format_html

from .models import *


class ProductDescriptionAdmin(TranslatableTabularInline):
    model = ProductDescription


class ProductImageAdmin(admin.StackedInline):
    model = ProductImage



class ProductAdmin(TranslatableAdmin):
    search_fields = ["title", "price"]
    list_display = ["title", "category", "price", "is_available"]
    
    class Media:
        js = ("js/countries_toggle.js",)
        css = {
            "all": ("css/countries.css",)
        }

    inlines = [
        ProductDescriptionAdmin,
        ProductImageAdmin,
    ]

    def get_prepopulated_fields(self, request, obj=None):
        return {"slug": ("title",)}


class CategoryAdminForm(MPTTAdminForm, TranslatableModelForm):
    pass

class CategoryAdmin(TranslatableAdmin, MPTTModelAdmin):
    form = CategoryAdminForm

    def get_prepopulated_fields(self, request, obj=None):
        return {"slug": ("title",)}


class SocialMediaLinkForm(forms.ModelForm):
    color = forms.CharField(widget=forms.TextInput(attrs={"type": "color"}))
    onHover = forms.CharField(widget=forms.TextInput(attrs={"type": "color"}))

    class Meta:
        model = SocialMediaLink
        fields = "__all__"


class SocialMediaLinkAdmin(admin.ModelAdmin):
    model = SocialMediaLink
    form = SocialMediaLinkForm


class ClientAdmin(admin.ModelAdmin):
    list_display = ["id", "balance"]


class ColorForm(TranslatableModelForm):
    hex_code = forms.CharField(widget=forms.TextInput(attrs={"type": "color"}))
    # title = forms.CharField(widget=forms.TextInput(attrs={"type": "text"}))

    class Meta:
        model = Color
        fields = "__all__"


class ColorAdmin(TranslatableAdmin):
    model = Color
    form = ColorForm
    list_display = ["title", "hex_code", "get_hex_code"]

    def get_hex_code(self, obj):
        return format_html(f"""<div style='
            display: block;
            width: 20px;
            height: 20px;
            background-color: {obj.hex_code};
        '></div>""")


admin.site.register(Product, ProductAdmin)
admin.site.register(ProductRating)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Banner, TranslatableAdmin)
admin.site.register(ProductDetail, TranslatableAdmin)
admin.site.register(FeatureGroup, TranslatableAdmin)
admin.site.register(Seller, admin.ModelAdmin)
admin.site.register(SocialMediaLink, SocialMediaLinkAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Discount)
admin.site.register(Size, TranslatableAdmin)
admin.site.register(Color, ColorAdmin)