from django.contrib import admin

from clients.models import Order, OrderProduct


class OrderAdmin(admin.ModelAdmin):
    list_display = ["id", "get_total_price"]

    def get_total_price(self, obj):
        return obj.total_price_in_sum


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderProduct)