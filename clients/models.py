from django.db import models
from dynamic_preferences.registries import global_preferences_registry

from products.models import Client, Color, Product, Size


global_preferences = global_preferences_registry.manager()


class CreateUpdateDateTimeMixin:
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Order(models.Model, CreateUpdateDateTimeMixin):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="orders")
    fullname = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    address_line = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    postcode = models.CharField(max_length=50)
    STATUS = [
        ("UNPAID", "UNPAID"),
        ("ACCEPTED", "ACCEPTED"),
        ("SHIPPING", "SHIPPING"),
        ("DELIVERED", "DELIVERED"),
        ("FINISHED", "FINISHED"),
    ]
    status = models.CharField(default="UNPAID", choices=STATUS, max_length=255)
    is_active = models.BooleanField(default=True)
    is_paid = models.BooleanField(default=False)

    @property
    def total_price_in_sum(self):
        usd_to_uzs = global_preferences["general__usd_to_uzs"]
        _sum = 0
        for item in self.order_items.all():
            _sum += item.quantity * item.price * usd_to_uzs
        return int(_sum)


class OrderProduct(models.Model, CreateUpdateDateTimeMixin):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order_items")
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, related_name="order_items")
    color = models.ForeignKey(Color, on_delete=models.SET_NULL, null=True)
    size = models.ForeignKey(Size, on_delete=models.SET_NULL, null=True)
    quantity = models.PositiveIntegerField(default=1)
    price = models.FloatField(default=0.0)
    
    def save(self, *args, **kwargs):
        if self.order.status == "UNPAID":
            self.price = self.product.price
        super(OrderProduct, self).save(*args, **kwargs)


class Address(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="address")
    fullname = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    address_line = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    city = models.CharField(max_length=255)

    def __str__(self):
        return self.fullname


class Email(models.Model):
    email_address = models.CharField(max_length=255)
    country_code = models.CharField(max_length=10)
    country_en = models.CharField(max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email_address